import React, { useEffect } from "react";
import withLayout from "../components/Layout";
import Jumbotron from "../components/Jumbotron";
import FeaturesContent from "../components/FeaturesContent";
// import "aos/dist/aos.css";
import AOS from "aos";

const props = {
  heading: "Why I built this site ?",
  description:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua" +
    "Eget lorem dolor sed viverra ipsum nunc. Sit amet facilisis magna etiam tempor. Id velit ut tortor pretium viverra suspendisse" +
    "potenti nullam ac. Amet purus gravida quis blandit turpis cursus in. Nunc eget lorem dolor sed viverra ipsum nunc aliquet bibendum." +
    "In pellentesque massa placerat duis ultricies lacus. Sed enim ut sem viverra aliquet eget sit amet. Tellus in hac habitasse platea" +
    "dictumst. Diam quis enim lobortis scelerisque fermentum dui. Fringilla urna porttitor rhoncus dolor purus. Ipsum suspendisse ultrices" +
    "gravida dictum. Ut morbi tincidunt augue interdum velit euismod in pellentesque massa. Dolor morbi non  risus quis varius. Elementum" +
    "facilisis leo vel fringilla est. Eget est lorem ipsum dolor.",
  headingClass: "rev-block why",
  descriptionClass: "desc animated zoomIn"
};
const About = () => {
  useEffect(() => {
    let init = [];
let x = setInterval(function() {
    init.push(AOS.init());
    if (init.length >= 2) {
         clearInterval(x);
    }
}, 1000);
    console.log("add aos here");
  }, []);
  return (
    <div>
      <Jumbotron props={props} />
      <FeaturesContent />
    </div>
  );
};
export default withLayout(About);
