import withLayout from "../components/Layout";
import Jumbotron from "../components/Jumbotron";
import IndexPageContent from "../components/IndexPageContent";
const props = {
  heading: "Love working remotely ?",
  description:
    "Here's a list of the best remote jobs available in the EMEA region",
  headingClass: "rev-block",
  descriptionClass: "animated job-desc fadeIn"
};

const Index = () => (
  <div>
    <Jumbotron props={props} />
    <IndexPageContent />
  </div>
);

export default withLayout(Index);
