const formatDate = job => {
  const month = job[1];
  const date = job[2];
  return month + " " + date;
};
export default formatDate;
