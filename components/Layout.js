import Head from "next/head";
import Header from "./Header";
import Footer from "./Footer";
// import "aos/dist/aos.css";


const withLayout = Page => props => {
  return (
  <div>
    <Head>
      <title>EMEA jobs</title>
      <link href="/app.css" rel="stylesheet" />
      <link href="https://unpkg.com/aos@3.0.0-beta.6/dist/aos.css" rel="stylesheet"/>
    </Head>
    <Header />
    <Page />
    <Footer />
  </div>
  )
}
export default withLayout;
