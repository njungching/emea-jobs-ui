const Image = ({props}) => (
  <div className="about-image" >
    <img className="img responsive" data-aos={props.aos} src="globe.jpg" width="1052px" />
  </div>
);

export default Image;
