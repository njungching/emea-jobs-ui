import Feature from "./Feature";
const features = [
  {
    image: "filter.svg",
    title: "Filtering",
    description: "erererer",
    aos: "fade-up"
  },
  {
    image: "bell.svg",
    title: "Job Alerts",
    description: "ererererererer",
    aos: "zoom-in-up"
  },
  {
    image: "edit-3.svg",
    title: "Post Jobs",
    description: "erer",
    aos: "fade-left"
  }
];

const renderFeatures = () => {
  let markup = [];
  features.map((feature, index) => {
    markup.push(<Feature key={index} props={feature} />);
  });
  return markup;
};
const Features = () => <div className="features">{renderFeatures()}</div>;
export default Features;
