const JumbotronContent = ({props}) => (
    <div className="jumbotron">
      <h1
        className={props.headingClass}
        data-aos={props.headingAOS ? props.headingAOS : ""}
      >
        <span>{props.heading}</span>
      </h1>
      <p
        className={props.descriptionClass}
        data-aos={props.descriptionAOS ? props.descriptionAOS : ""}
      >
        {props.description}{" "}
      </p>
    </div>
)
export default JumbotronContent