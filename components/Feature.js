const Feature = ({ props }) => (
  <div className="test" data-aos={props.aos}>
    <span className="feature-icon">
      <img className="feature-icon-img" src={props.image} width="50px" />
    </span>
    <span className="feature-content">
      <p className="feature-title">{props.title}</p>
      <p className="feature-desc">{props.description}</p>
    </span>
  </div>
);
export default Feature;
