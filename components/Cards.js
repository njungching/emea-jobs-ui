import Card from "./Card";
import jobs from "../utils/data";
console.log(jobs);

const Cards = () => (
  <div className="cards animated zoomIn">
    {jobs.map((job, index) => {
      return <Card key={index} job={job} />;
    })}
  </div>
);
export default Cards;
