import Link from "next/link";
const Header = () => (
  <div className="grid">
    <div className="navbar">
      <a className="logo" href="#">
        <span>EMEA Jobs</span>
      </a>
      <ul>
        <Link href="/">
          <li>Home</li>
        </Link>
        <Link href="/about">
          <li>About</li>
        </Link>
      </ul>
    </div>
  </div>
);

export default Header;
