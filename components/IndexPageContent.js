import JobsHeading from "./JobsHeading";
import Cards from "./Cards";
const IndexPageContent = () => (
  <div className="grid jobs">
    <JobsHeading />
    <Cards />
  </div>
);
export default IndexPageContent;
