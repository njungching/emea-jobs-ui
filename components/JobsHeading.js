const JobsHeading = () => (
  <div className="job-heading">
    <h3 className="animated slideInLeft latest-jobs-heading">
      Latest Remote Jobs
    </h3>
    <div className="animated fadeInRightBig job-heading-border"></div>
  </div>
);
export default JobsHeading;
