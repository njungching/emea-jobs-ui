import formatDate from "../utils/FormateDate";
const Card = ({ job }) => (
  <div className="card">
    <div className="one">
      <span className="details job-title">{job.jobTitle}</span>
      <span className="details job-company">{job.company}</span>
      <span className="details job-location">{job.location}</span>
    </div>
    <div className="two">
      <span className="date-posted">
        {formatDate(job.datePosted.split(" "))}
      </span>
      <span>
        <a href={job.linkToApply} target="_blank" className="btn">
          Apply
        </a>
      </span>
    </div>
  </div>
);

export default Card;
