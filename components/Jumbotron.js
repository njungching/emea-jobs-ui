import JumbotronContent from "./JumbotronContent"
const Jumbotron = ({ props }) => (
  <div className="grid">
    <JumbotronContent props={props}/>
    </div>

);
export default Jumbotron;
