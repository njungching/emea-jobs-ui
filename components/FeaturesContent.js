import React, { useEffect } from "react";
import Image from "./Image";
import JumbotronContent from "./JumbotronContent";
import Features from "./Features";
import AOS from "aos";

const props = {
  heading: "Creating more value",
  description:
    "I'd love to make this site more useful and these are some ways more " +
    "value can be created. Some features I plan to add soon",
  headingClass: "value-h1",
  descriptionClass: "sub",
  headingAOS: "fade-left",
  descriptionAOS: "fade-right"
};

const FeaturesContent = () => {
  useEffect(() => {
    AOS.refresh();
    console.log("add aos here");
  }, []);
  return (
    <div className="test-1">
       <div className="about-image" data-aos="fade-up">
    <img className="img responsive"  src={"/globe.jpg"} />
    {/* <h1>image</h1> */}
  </div>
      <JumbotronContent props={props} />
      <Features />
    </div>
  );
};
export default FeaturesContent;
